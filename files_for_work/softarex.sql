-- phpMyAdmin SQL Dump
-- version 4.0.10.6
-- http://www.phpmyadmin.net
--
-- Хост: 127.0.0.1:3306
-- Время создания: Ноя 28 2016 г., 23:24
-- Версия сервера: 5.6.22-log
-- Версия PHP: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `softarex`
--

-- --------------------------------------------------------

--
-- Структура таблицы `applications`
--

CREATE TABLE IF NOT EXISTS `applications` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app_name` varchar(50) NOT NULL,
  `vendor_name` varchar(50) NOT NULL,
  `license_required` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Дамп данных таблицы `applications`
--

INSERT INTO `applications` (`id`, `app_name`, `vendor_name`, `license_required`) VALUES
(4, 'Register', 'Microsoft', 'Free'),
(5, 'Google maps', 'Google', 'Free'),
(6, 'Wiki', 'No name', 'Free');

-- --------------------------------------------------------

--
-- Структура таблицы `computers`
--

CREATE TABLE IF NOT EXISTS `computers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `computer_name` varchar(50) NOT NULL,
  `ip_address` varchar(30) NOT NULL,
  `login` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Дамп данных таблицы `computers`
--

INSERT INTO `computers` (`id`, `computer_name`, `ip_address`, `login`, `password`) VALUES
(4, 'Computer_238', '192.168.0.52', 'comp', 'qwerty'),
(5, 'Computer_125', '192.168.0.121', 'my_comp', 'tuktuk'),
(6, 'Computer_784', '192.168.0.33', 'log_2', '4411');

-- --------------------------------------------------------

--
-- Структура таблицы `computer_app`
--

CREATE TABLE IF NOT EXISTS `computer_app` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `computer_id` int(11) NOT NULL,
  `app_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_computer_attr_applications` (`app_id`),
  KEY `FK_computer_attr_computers` (`computer_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=14 ;

--
-- Дамп данных таблицы `computer_app`
--

INSERT INTO `computer_app` (`id`, `computer_id`, `app_id`) VALUES
(7, 4, 4),
(8, 4, 5),
(9, 4, 6),
(10, 6, 5),
(12, 6, 6),
(13, 5, 5);

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `password` varchar(60) NOT NULL,
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `role` tinyint(1) NOT NULL DEFAULT '1',
  `is_locked` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=24 ;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `first_name`, `last_name`, `role`, `is_locked`) VALUES
(21, 'admin', '$2y$14$L.Y06LIt/R8UA8nussXM9.U5qJe1N.oCKwAm442vMNLGl9z.vQTIC', 'System', 'Admin', 2, 0),
(22, 'apple', '$2y$14$4XcHEEdwUl4WkF261M.Yne/v4q71aIptBrwUqEa0iSVq8vzbBgXsS', 'Steve', 'Jobs', 1, 0),
(23, 'microsoft', '$2y$14$YzmVZrmp8AFUN1AqJfc9b.SBzk9ydrbtKiX1iBZot5EndfUVPwDkS', 'Bill', 'Gates', 1, 0);

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `computer_app`
--
ALTER TABLE `computer_app`
  ADD CONSTRAINT `FK_computer_attr_applications` FOREIGN KEY (`app_id`) REFERENCES `applications` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_computer_attr_computers` FOREIGN KEY (`computer_id`) REFERENCES `computers` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
