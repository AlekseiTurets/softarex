<?php

/**
 * This is the model class for table "computer_app".
 *
 * The followings are the available columns in table 'computer_app':
 * @property integer $id
 * @property integer $computer_id
 * @property integer $app_id
 *
 * The followings are the available model relations:
 * @property Applications $app
 * @property Computers $computer
 */
class ComputerApp extends SupportModel
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'computer_app';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('computer_id, app_id', 'required'),
			array('computer_id, app_id', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, computer_id, app_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'app' => array(self::BELONGS_TO, 'Applications', 'app_id'),
			'computer' => array(self::BELONGS_TO, 'Computers', 'computer_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'computer_id' => 'Computer',
			'app_id' => 'Application',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('computer_id',$this->computer_id);
		$criteria->compare('app_id',$this->app_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Генерирует поля для таблицы
	 * @return array
	 */
	public function columns()
	{
		$columns = array(
			'id' => $this->columnId(),
			'computer_id' => array(
				'name' => 'computer_id',
				'filter' => Computers::listData(), // if database not big
				'type' => 'html',
				'value' => 'CHtml::link($data->computer->computer_name, array("Computers/admin", "Computers[id]" => $data->computer_id))',
			),
			'app_id' => array(
				'name' => 'app_id',
				'filter' => Applications::listData(), // if database not big
				'type' => 'html',
				'value' => 'CHtml::link($data->app->app_name, array("Applications/admin", "Applications[id]" => $data->app_id))',
			),
		);
		if(SupportController::isAdmin()){
			$columns[] = $this->moduleCButtonColumn($this->getDeleteConfirmation());
		}
		return $columns;
	}

	/**
	 * Получить уведомление на удаление
	 * @return string
	 */
	private function getDeleteConfirmation()
	{
		return "js:'".Support::$del_confirm_msg_short." ComputerApp relation '+".$this->getCGridViewContent(2)."+'-'+".$this->getCGridViewContent(3)."+'?'";
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ComputerApp the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
