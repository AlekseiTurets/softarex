<?php

/**
 * This is the model class for table "applications".
 *
 * The followings are the available columns in table 'applications':
 * @property integer $id
 * @property string $app_name
 * @property string $vendor_name
 * @property string $license_required
 *
 * The followings are the available model relations:
 * @property computerApp[] $computerApps
 */
class Applications extends SupportModel
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'applications';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('app_name, vendor_name, license_required', 'required'),
			array('app_name, vendor_name, license_required', 'length', 'max'=>50),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, app_name, vendor_name, license_required', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'computerApps' => array(self::HAS_MANY, 'ComputerApp', 'app_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'app_name' => 'App Name',
			'vendor_name' => 'Vendor Name',
			'license_required' => 'License Required',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('app_name',$this->app_name,true);
		$criteria->compare('vendor_name',$this->vendor_name,true);
		$criteria->compare('license_required',$this->license_required,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Генерирует поля для таблицы
	 * @return array
	 */
	public function columns()
	{
		$columns = array(
			'id' => $this->columnId(),
			'app_name',
			'vendor_name',
			'license_required'
		);
		if(SupportController::isAdmin()){
			$columns[] = $this->moduleCButtonColumn($this->getDeleteConfirmation());
		}
		return $columns;
	}

	/**
	 * Получить уведомление на удаление
	 * @return string
	 */
	private function getDeleteConfirmation()
	{
		return "js:'".Support::$del_confirm_msg_short." '+".$this->getCGridViewContent(2)."+'?'";
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Applications the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return array
     */
	public static function listData()
	{
		return CHtml::listData(Applications::model()->findAll(array('select' => array('id', 'app_name'))), 'id', 'app_name');
	}
}
