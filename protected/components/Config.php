<?php

class Config
{
    //code
    const CODE_TRUE = 1;
    const CODE_FALSE = 0;

    //path
    const DOMAIN = 'http://softarex';
    const DIR_PROJECT = '';
    const DIR_IMAGES = '/images';

    //images
    const IMG_ALT = 'Изображение отсутствует';
    const IMG_LOGO = 'web.jpg';
}