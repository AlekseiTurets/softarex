<?php

class Url
{
    /**
     * Строит url
     * @param $relative_path
     * @return string
     */
    public static function buildUrl($relative_path)
    {
        return Config::DOMAIN.Config::DIR_PROJECT.$relative_path;
    }

    /**
     * Строит относительный путь
     * @param $relative_path
     * @return string
     */
    public static function buildLocalPath($relative_path)
    {
        return $_SERVER['DOCUMENT_ROOT'].Config::DIR_PROJECT.$relative_path;
    }
}