<?php

class Support
{
    private static $yes_no = array('0' => 'No', '1' => 'Yes');
    public static $admin_note = "<p class=\"p_admin\">You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.</p>";
    public static $form_note = "<p class=\"note\">Fields with <span class=\"required\">*</span> are required.</p>";
    public static $del_confirm_msg = 'Are you sure you want to delete this item?';
    public static $del_confirm_msg_short = 'Are you sure you want to delete';

    /**
     * @param bool|false $key
     * @return array
     */
    public static function getYesNo($key = false)
    {
        return $key !== false ? self::$yes_no[$key] : self::$yes_no;
    }

    /**
     * Очищает от пробелов, табуляций и убирает теги
     * @param $data
     * @return string
     */
    public static function clear($data)
    {
        return trim(strip_tags($data));
    }

    /**
     * @param string $text
     * @return array
     */
    public static function getEmptyField($text = 'No')
    {
        return array('empty' => $text);
    }
}