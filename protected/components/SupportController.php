<?php

class SupportController extends Controller
{
    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout='//layouts/column3';
    public $menu_h = array();

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * пїЅпїЅпїЅпїЅпїЅ пїЅпїЅпїЅ пїЅпїЅпїЅпїЅпїЅпїЅпїЅпїЅ
     * @param $rules
     * @return array
     */
    protected function moduleRules($rules = array())
    {
        $rules[] = array('allow',
            'actions'=>array('admin'),
            'controllers'=>array('Users'),
            'expression' => 'SupportController::isAdmin()'
        );
        $rules[] = array('allow',
            'actions'=>array('create','update','delete'),
            'expression' => 'SupportController::isAdmin()'
        );
        $rules[] = array('allow',
            'actions'=>array('admin'),
            'controllers'=>array('Applications', 'Computers', 'ComputerApp'),
            'users'=>array('@'),
        );
        $rules[] = array('allow',
            'actions'=>array('ExportCSV'),
            'users'=>array('@'),
        );
        $rules[] = array('deny',  // deny all users
            'users'=>array('*'),
        );
        return $rules;
    }

    /**
     * CSV СЌРєСЃРїРѕСЂС‚
     * @param $model_name
     */
    protected function moduleActionExportCSV($model_name) {
        /* @var $model Users|Computers|Applications */
        /* @var $model_name CActiveRecord */

        $models = $model_name::model()->findAll();
        if(!empty($models)){
            $data = $this->_exportCSV(array_keys(current($models)->getAttributes()));
            foreach($models as $model)
            {
                $data .= $this->_exportCSV($model->getAttributes());
            }
            header('Content-type: text/csv');
            header('Content-Disposition: attachment; filename="export_' . date('d.m.Y') . '.csv"');
            echo $data;
            //echo iconv('utf-8', 'windows-1251', $data);
        }else{
            $this->redirect(array($model_name.'/admin'));
        }
    }

    /**
     * @param $attributes
     * @return string
     */
    protected function _exportCSV($attributes)
    {
        $data = '';
        foreach($attributes as $attribute)
        {
            $data .= $attribute.';';
        }
        return $data."\r\n";
    }

    /**
     * пїЅпїЅпїЅпїЅпїЅпїЅпїЅпїЅпїЅпїЅ actionCreate
     * @param $model_name
     */
    protected function moduleActionCreate($model_name)
    {
        /* @var $model Users|Computers|Applications|ComputerApp */
        $model=new $model_name;

        if(isset($_POST[$model_name]))
        {
            $model->attributes=$_POST[$model_name];
            if($model instanceof Users){
                $model->passwordHash();
            }
            if($model->save()){
                $this->redirect(array($model_name.'/admin'));
            }
        }

        $this->render('create',array(
            'model'=>$model,
        ));
    }

    /**
     * пїЅпїЅпїЅпїЅпїЅпїЅпїЅпїЅпїЅпїЅ actionUpdate
     * @param $id
     * @param $model_name
     */
    protected function moduleActionUpdate($id, $model_name)
    {
        $model=$this->moduleLoadModel($id, $model_name);

        if(isset($_POST[$model_name]))
        {
            $model->attributes=$_POST[$model_name];
            if($model instanceof Users){
                $model->passwordHash();
            }
            if($model->update()){
                $this->redirect(array($model_name.'/admin'));
            }
        }

        $this->render('update',array(
            'model'=>$model,
        ));
    }

    /**
     * пїЅпїЅпїЅпїЅпїЅпїЅпїЅпїЅпїЅпїЅ actionAdmin
     * @param $model_name
     */
    protected function moduleActionAdmin($model_name)
    {
        /* @var $model SupportModel */
        $model=new $model_name('search');
        $model->unsetAttributes();  // clear any default values
        if(isset($_GET[$model_name])){
            $model->attributes=$_GET[$model_name];
        }

        $this->render('admin',array(
            'model'=>$model,
        ));
    }

    /**
     * пїЅпїЅпїЅпїЅпїЅпїЅпїЅпїЅпїЅпїЅ actionDelete
     * @param $id
     * @param $model_name
     */
    protected function moduleActionDelete($id, $model_name)
    {
        $model = $this->moduleLoadModel($id, $model_name);
        $model->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if(!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array($model_name.'/admin'));
    }

    /**
     * пїЅпїЅпїЅпїЅпїЅпїЅпїЅпїЅпїЅ пїЅпїЅпїЅпїЅпїЅпїЅ
     * @param $id
     * @param $model_name
     * @return array|mixed|null|static
     * @throws CHttpException
     */
    protected function moduleLoadModel($id, $model_name)
    {
        /* @var $model_name CActiveRecord */
        $model=$model_name::model()->findByPk($id);
        if($model===null)
            throw new CHttpException(404,'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param CActiveRecord $model the model to be validated
     * @param $post_ajax
     */
    protected function modulePerformAjaxValidation($model, $post_ajax)
    {
        if(isset($_POST['ajax']) && $_POST['ajax']===$post_ajax)
        {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    /**
     * пїЅпїЅпїЅпїЅпїЅпїЅпїЅпїЅпїЅ пїЅпїЅпїЅпїЅпїЅ пїЅпїЅпїЅ пїЅпїЅпїЅ
     * @return bool
     */
    public static function isAdmin()
    {
        return Yii::app()->user->role == Users::ROLE_ADMIN;
    }
}