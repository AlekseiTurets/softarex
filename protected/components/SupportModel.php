<?php

class SupportModel extends CActiveRecord
{
    public static $action_current;
    public static $action_next;

    /**
     * @return array
     */
    public function columnId()
    {
        return array(
            'name' => 'id',
            'type' => 'html',
            'value' => '$data->id',
            'htmlOptions' => array(
                'class' => 'CGridView_50'
            ),
        );
    }

    /**
     * Получить контент CGridView
     * @param $child_id
     * @return string
     */
    protected function getCGridViewContent($child_id)
    {
        return "$(this).parent().parent().children(':nth-child(".$child_id.")').text()";
    }

    /**
     * @param $deleteConfirmation
     * @return array
     */
    protected function moduleCButtonColumn($deleteConfirmation)
    {
        return array(
            'class'=>'CButtonColumn',
            'template' => '{update}{delete}',
            'deleteConfirmation' => $deleteConfirmation
        );
    }

    /**
     * Получить url другой формы
     * @return string
     */
    public static function getActionUrl()
    {
        return Url::buildUrl('/index.php/Users/'.self::$action_next);
    }
}