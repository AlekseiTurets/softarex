<?php /* @var $this SupportController */ ?>
<?php $this->beginContent('//layouts/main'); ?>
    <div id="header">
        <div id="img_logo">
            <img src="<?php echo Config::DIR_IMAGES.'/'.Config::IMG_LOGO; ?>" alt="<?php echo Config::IMG_ALT; ?>">
        </div>
        <div id="logo">
            <?php echo CHtml::encode(Yii::app()->name); ?>
        </div>
    </div><!-- header -->
    <div>
        <div id="Widget_main_menu">
            <?php $this->widget('application.extensions.main_menu.WidgetMenu', array(
                'render' => '_main_menu',
                'items' => array(
                    array('label'=>'Users', 'url'=>'Users/admin', 'visible' => SupportController::isAdmin()),
                    array('label'=>'Computers', 'url'=>'Computers/admin'),
                    array('label'=>'Applications', 'url'=>'Applications/admin'),
                    array('label'=>'ComputerApp', 'url'=>'ComputerApp/admin'),
                    array('label'=>'Logout ('.Yii::app()->user->name.')', 'url'=>'Users/logout'),
                )
            )); ?>
        </div><!-- sidebar -->
        <div id="content" class="column_3">
            <?php if(SupportController::isAdmin()): ?>
                <div id="Widget_menu_h">
                    <?php $this->widget('application.extensions.main_menu.WidgetMenu', array(
                        'render' => '_menu_h',
                        'items' => $this->menu_h
                    )); ?>
                </div><!-- sidebar -->
            <?php endif; ?>
            <?php echo $content; ?>
        </div><!-- content -->
    </div>
<?php $this->endContent(); ?>