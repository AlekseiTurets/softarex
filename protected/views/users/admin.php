<?php
/* @var $this UsersController */
/* @var $model Users */

$this->menu_h=array(
	array('label'=>'Add user', 'url'=>'Users/create'),
);
$dataProvider = $model->search();
?>

<h1>Manage users</h1>

<?php echo Support::$admin_note;
$this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'users-grid',
	'dataProvider'=>$dataProvider,
	'filter'=>$model,
	'columns'=>$model->columns()
)); ?>

<?php if($dataProvider->itemCount > Config::CODE_FALSE){
	$this->renderPartial('/support/export_csv_link', array('model_name' => get_class($model)));
} ?>