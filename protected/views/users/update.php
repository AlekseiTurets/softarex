<?php
/* @var $this UsersController */
/* @var $model Users */

$this->menu_h=array(
	array('label'=>'Add user', 'url'=>'Users/create'),
	array('label'=>'Manage users', 'url'=>'Users/admin'),
	array('label'=>'Delete user', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>Support::$del_confirm_msg_short.' user '.$model->first_name.' '.$model->last_name.'?'))
);
?>

<h1>Update user <?php echo $model->first_name.' '.$model->last_name; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>