<?php
/* @var $this UsersController */
/* @var $model Users */

$this->menu_h=array(
	array('label'=>'Manage users', 'url'=>'Users/admin'),
);
?>

<h1>Add user</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>