<?php
/* @var $this UsersController */
/* @var $model Users|LoginForm */
/* @var $form CActiveForm */
?>

<div class="form">

    <?php $form=$this->beginWidget('CActiveForm', array(
        'id'=>'users-form',
        'enableAjaxValidation'=>false,
    )); ?>

    <?php echo Support::$form_note; ?>
    <?php echo $form->errorSummary($model, null, null, array('id' => 'auth_error_summary')); ?>

    <div class="row">
        <div class="form_label_div">
            <?php echo $form->labelEx($model,'username'); ?>
        </div>
        <div class="form_input_div">
            <?php echo $form->textField($model,'username',array('maxlength'=>50)); ?>
            <?php echo $form->error($model,'username'); ?>
        </div>
    </div>

    <div class="row">
        <div class="form_label_div">
            <?php echo $form->labelEx($model,'password'); ?>
        </div>
        <div class="form_input_div">
            <?php echo $form->passwordField($model,'password',array('maxlength'=>60)); ?>
            <?php echo $form->error($model,'password'); ?>
        </div>
    </div>

    <?php if($model instanceof Users): ?>
        <div class="row">
            <div class="form_label_div">
                <?php echo $form->labelEx($model,'first_name'); ?>
            </div>
            <div class="form_input_div">
                <?php echo $form->textField($model,'first_name',array('maxlength'=>50)); ?>
                <?php echo $form->error($model,'first_name'); ?>
            </div>
        </div>

        <div class="row">
            <div class="form_label_div">
                <?php echo $form->labelEx($model,'last_name'); ?>
            </div>
            <div class="form_input_div">
                <?php echo $form->textField($model,'last_name',array('maxlength'=>50)); ?>
                <?php echo $form->error($model,'last_name'); ?>
            </div>
        </div>
    <?php endif; ?>

    <div class="row buttons auth_button">
        <?php echo CHtml::link(SupportModel::$action_next, SupportModel::getActionUrl(), array('class' => 'auth_action')); ?>
        <?php echo CHtml::submitButton(SupportModel::$action_current); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->