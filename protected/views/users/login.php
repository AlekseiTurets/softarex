<?php
/* @var $this UsersController */
/* @var $model LoginForm */
?>

<div class="auth_wrapper">
	<h1><?php echo SupportModel::$action_current; ?></h1>

	<?php $this->renderPartial('_form_auth', array('model' => $model)); ?>
</div>