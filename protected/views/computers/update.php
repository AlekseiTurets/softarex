<?php
/* @var $this ComputersController */
/* @var $model Computers */

$this->menu_h=array(
	array('label'=>'Add computer', 'url'=>'Computers/create'),
	array('label'=>'Manage computers', 'url'=>'Computers/admin'),
	array('label'=>'Delete computer', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>Support::$del_confirm_msg_short.' computer '.$model->computer_name.'?'))
);
?>

<h1>Update computer <?php echo $model->computer_name; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>