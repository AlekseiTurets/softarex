<?php
/* @var $this ComputersController */
/* @var $model Computers */

$this->menu_h=array(
	array('label'=>'Add computer', 'url'=>'Computers/create'),
);
$dataProvider = $model->search();
?>

<h1>Manage computers</h1>

<?php echo Support::$admin_note;
$this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'computers-grid',
	'dataProvider'=>$dataProvider,
	'filter'=>$model,
	'columns'=>$model->columns()
)); ?>

<?php if($dataProvider->itemCount > Config::CODE_FALSE){
	$this->renderPartial('/support/export_csv_link', array('model_name' => get_class($model)));
} ?>