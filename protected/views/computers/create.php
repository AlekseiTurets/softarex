<?php
/* @var $this ComputersController */
/* @var $model Computers */

$this->menu_h=array(
	array('label'=>'Manage computers', 'url'=>'Computers/admin'),
);
?>

<h1>Add computer</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>