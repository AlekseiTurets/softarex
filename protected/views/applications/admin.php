<?php
/* @var $this ApplicationsController */
/* @var $model Applications */

$this->menu_h=array(
	array('label'=>'Add application', 'url'=>'Applications/create'),
);
$dataProvider = $model->search();
?>

<h1>Manage applications</h1>

<?php echo Support::$admin_note;
$this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'applications-grid',
	'dataProvider'=>$dataProvider,
	'filter'=>$model,
	'columns'=>$model->columns()
)); ?>

<?php if($dataProvider->itemCount > Config::CODE_FALSE){
	$this->renderPartial('/support/export_csv_link', array('model_name' => get_class($model)));
} ?>