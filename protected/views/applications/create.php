<?php
/* @var $this ApplicationsController */
/* @var $model Applications */

$this->menu_h=array(
	array('label'=>'Manage applications', 'url'=>'Applications/admin'),
);
?>

<h1>Add application</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>