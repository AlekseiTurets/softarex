<?php
/* @var $this ApplicationsController */
/* @var $model Applications */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'applications-form',
	'enableAjaxValidation'=>false,
)); ?>

	<?php echo Support::$form_note; ?>
	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'app_name'); ?>
		<?php echo $form->textField($model,'app_name',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'app_name'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'vendor_name'); ?>
		<?php echo $form->textField($model,'vendor_name',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'vendor_name'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'license_required'); ?>
		<?php echo $form->textField($model,'license_required',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'license_required'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Add' : 'Update'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->