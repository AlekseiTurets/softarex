<?php
/* @var $this ApplicationsController */
/* @var $model Applications */

$this->menu_h=array(
	array('label'=>'Add application', 'url'=>'Applications/create'),
	array('label'=>'Manage applications', 'url'=>'Applications/admin'),
	array('label'=>'Delete application', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>Support::$del_confirm_msg_short.' application '.$model->app_name.'?'))
);
?>

<h1>Update Application <?php echo $model->app_name; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>