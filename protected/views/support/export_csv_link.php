<?php
/**
 * @var $model_name
 */
?>

<div class="export_csv">
    <?php echo CHtml::link('Export to CSV', Url::buildUrl('/index.php/'.$model_name.'/ExportCSV')) ?>
</div>
