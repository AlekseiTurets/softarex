<?php
/* @var $this ComputerAppController */
/* @var $model ComputerApp */

$this->menu_h=array(
	array('label'=>'Add ComputerApp', 'url'=>'ComputerApp/create'),
);
$dataProvider = $model->search();
?>

<h1>Manage ComputerApp relations</h1>

<?php echo Support::$admin_note;
$this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'computer-app-grid',
	'dataProvider'=>$dataProvider,
	'filter'=>$model,
	'columns'=>$model->columns()
)); ?>

<?php if($dataProvider->itemCount > Config::CODE_FALSE){
	$this->renderPartial('/support/export_csv_link', array('model_name' => get_class($model)));
} ?>