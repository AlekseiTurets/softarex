<?php
/* @var $this ComputerAppController */
/* @var $model ComputerApp */

$this->menu_h=array(
	array('label'=>'Manage ComputerApp', 'url'=>'ComputerApp/admin'),
);
?>

<h1>Add ComputerApp relation</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>