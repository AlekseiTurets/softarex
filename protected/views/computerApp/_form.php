<?php
/* @var $this ComputerAppController */
/* @var $model ComputerApp */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'computer-app-form',
	'enableAjaxValidation'=>false,
)); ?>

	<?php echo Support::$form_note; ?>
	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'computer_id'); ?>
		<?php echo $form->dropDownList($model,'computer_id',Computers::listData()); ?>
		<?php echo $form->error($model,'computer_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'app_id'); ?>
		<?php echo $form->dropDownList($model,'app_id',Applications::listData()); ?>
		<?php echo $form->error($model,'app_id'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Add' : 'Update'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->