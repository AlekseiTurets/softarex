<?php
/* @var $this ComputerAppController */
/* @var $model ComputerApp */

$this->menu_h=array(
	array('label'=>'Add ComputerApp', 'url'=>'ComputerApp/create'),
	array('label'=>'Manage ComputerApp', 'url'=>'ComputerApp/admin'),
	array('label'=>'Delete ComputerApp', 'url'=>'#', 'linkOptions'=>array(
		'submit'=>array('delete','id'=>$model->id),'confirm'=>Support::$del_confirm_msg_short.' ComputerApp relation '.$model->computer->computer_name.'-'.$model->app->app_name.'?')
	)
);
?>

<h1>Update ComputerApp relation <?php echo $model->computer->computer_name.'-'.$model->app->app_name; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>