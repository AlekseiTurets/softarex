<?php

class UsersController extends SupportController
{
	private $model_name = 'Users';

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		$rules[] = array('allow',
			'actions'=>array('registration', 'login', 'logout'),
			'users'=>array('*'),
		);
		return $this->moduleRules($rules);
	}

	/**
	 * Creates a new model.
	 */
	public function actionCreate()
	{
		$this->moduleActionCreate($this->model_name);
	}

	/**
	 * Updates a particular model.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$this->moduleActionUpdate($id, $this->model_name);
	}

	/**
	 * Deletes a particular model.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->moduleActionDelete($id, $this->model_name);
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$this->moduleActionAdmin($this->model_name);
	}

	/**
	 * Регистрация
     */
	public function actionRegistration()
	{
		/* @var $model Users */
		$this->layout = '//layouts/column1';
		SupportModel::$action_current = 'Registration';
		SupportModel::$action_next = 'Login';
		$model = new $this->model_name();

		if(isset($_POST[$this->model_name])){
			$model->setAttributes($_POST[$this->model_name]);
			if($model->validate()){
				$model->passwordHash();
				if($model->save(false)){
					$this->redirect(array($this->model_name.'/login'));
				}
			}
		}

		$this->render('registration',array(
			'model'=>$model,
		));
	}

	/**
	 * Авторизация
	 */
	public function actionLogin()
	{
		$this->layout = '//layouts/column1';
		SupportModel::$action_current = 'Login';
		SupportModel::$action_next = 'Registration';
		$model=new LoginForm;

		// if it is ajax validation request
		if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}

		// collect user input data
		if(isset($_POST['LoginForm']))
		{
			$model->attributes=$_POST['LoginForm'];
			// validate user input and redirect to the previous page if valid
			if($model->validate() && $model->login()){
				$this->redirect(array('Computers/admin'));
			}
		}
		// display the login form
		$this->render('login',array('model'=>$model));
	}

	/**
	 * Logs out the current user and redirect to homepage.
	 */
	public function actionLogout()
	{
		Yii::app()->user->logout();
		$this->redirect(Yii::app()->homeUrl);
	}

	/**
	 * CSV экспорт
     */
	public function actionExportCSV()
	{
		$this->moduleActionExportCSV($this->model_name);
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Users the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		return $this->moduleLoadModel($id, $this->model_name);
	}

	/**
	 * Performs the AJAX validation.
	 * @param Users $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		$this->modulePerformAjaxValidation($model, 'users-form');
	}
}
