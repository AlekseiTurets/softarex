<?php

class ComputersController extends SupportController
{
	private $model_name = 'Computers';

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return $this->moduleRules();
	}

	/**
	 * Creates a new model.
	 */
	public function actionCreate()
	{
		$this->moduleActionCreate($this->model_name);
	}

	/**
	 * Updates a particular model.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$this->moduleActionUpdate($id, $this->model_name);
	}

	/**
	 * Deletes a particular model.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->moduleActionDelete($id, $this->model_name);
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$this->moduleActionAdmin($this->model_name);
	}

	/**
	 * CSV экспорт
	 */
	public function actionExportCSV()
	{
		$this->moduleActionExportCSV($this->model_name);
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Computers the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		return $this->moduleLoadModel($id, $this->model_name);
	}

	/**
	 * Performs the AJAX validation.
	 * @param Computers $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		$this->modulePerformAjaxValidation($model, 'computers-form');
	}
}
