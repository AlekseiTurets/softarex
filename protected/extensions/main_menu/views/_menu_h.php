<?php
/* @var $items */
?>

<ul id="menu_h_ul">
    <?php foreach($items as $key => $item): ?>
        <li>
            <?php echo CHtml::link($item['label'], Url::buildUrl('/index.php/'.$item['url']), isset($item['linkOptions']) ? $item['linkOptions'] : array()); ?>
        </li>
    <?php endforeach; ?>
</ul>