<?php
/* @var $items */
?>

<ul id="mainmenu_ul">
    <?php foreach($items as $key => $item): ?>
        <?php $visible = isset($item['visible']) ? $item['visible'] : Config::CODE_TRUE; ?>
        <?php if($visible): ?>
            <li>
                <a href="<?php echo Url::buildUrl('/index.php/'.$item['url']); ?>"><?php echo $item['label']; ?></a>
            </li>
        <?php endif; ?>
    <?php endforeach; ?>
</ul>