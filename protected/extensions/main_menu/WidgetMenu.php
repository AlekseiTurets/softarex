<?php
Yii::import('zii.widgets.CMenu');

class WidgetMenu extends CMenu
{
    public $items;
    public $render;

    /**
     * Запуск виджета
     */
    public function run()
    {
        $this->render($this->render, array(
            'items' => $this->items
        ));
    }

    /**
     * Renders the data items for the view.
     * Each item is corresponding to a single data model instance.
     * Child classes should override this method to provide the actual item rendering logic.
     */
    public function renderItems()
    {
        // TODO: Implement renderItems() method.
    }
}